package com.soprabanking.medicine.util;

public interface GlobalConstants {
	
	public static final String MEDICINE_SEQUENCE = "medicine_sequence";
    public static final String DATABASE_SEQ_COLUMN_SEQ = "seq";
    public static final String DATABASE_SEQ_COLUMN_ID = "_id";    

}
