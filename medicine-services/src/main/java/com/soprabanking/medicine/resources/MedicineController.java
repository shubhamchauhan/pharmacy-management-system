package com.soprabanking.medicine.resources;

import java.util.Objects;

import javax.validation.Valid;

import org.apache.commons.lang.math.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soprabanking.medicine.exception.ResourceNotFoundException;
import com.soprabanking.medicine.model.Medicine;
import com.soprabanking.medicine.model.MedicineInfoEvent;
import com.soprabanking.medicine.repository.MedicineRepo;
import com.soprabanking.medicine.service.SequenceGeneratorService;
import com.soprabanking.medicine.util.GlobalConstants;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/pharmacy/medicine")
@CrossOrigin(origins = "http://localhost:4200")
public class MedicineController {
	// Adding default slf4j logging
	Logger log = LoggerFactory.getLogger(MedicineController.class);

	@Autowired
	private MedicineRepo repository;

	@Autowired
	private SequenceGeneratorService sequenceGeneratorService;

	@Autowired
	private KafkaTemplate<String, MedicineInfoEvent> kafkaTemplate;

	@PostMapping("/addMedicine")
	public Mono<Medicine> saveMedicine(@Valid @RequestBody Medicine medicine) {
		medicine.setId(sequenceGeneratorService.generateSequence(GlobalConstants.MEDICINE_SEQUENCE));
		log.info("Medicine: " + medicine.getMedicineName() + " has been added with id" + medicine.getId());
		return repository.save(medicine);
	}

	@GetMapping("/getAllMedicines")
	public Flux<Medicine> getAllMedicine() {
		log.info("Medicine list is asked to be retrieved");
		return repository.findAll();
	}

	@GetMapping("/getMedicine/{id}")
	public Mono<Medicine> getMedicine(@PathVariable int id) throws ResourceNotFoundException {
		log.info("Retrieving Medicine with id: " + id);
		return Mono.just(id)
				.flatMap(repository::findById)
				.switchIfEmpty(Mono.error(new ResourceNotFoundException("No medicine found for this id :: " + id)));
	}

	@PutMapping("/editMedicine/{id}")
	public Mono<Medicine> updateMedicine(@PathVariable int id, @Valid @RequestBody Medicine medicine)
			throws ResourceNotFoundException {
		log.info("Update Medicine with id: " + id);
		Mono<Medicine> existingMedicine = repository.findById(id);
		if (Objects.isNull(existingMedicine))
			return Mono.error(new ResourceNotFoundException("No medicine found for this id :: " + id));
		return repository.save(medicine);
	}

	@DeleteMapping("/deleteMedicine/{id}")
	public Mono<ResponseEntity<Void>> deleteMedicine(@PathVariable int id) {
		log.info("Delete Medicine with id: " + id);
		return repository.deleteById(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK)));
	}

	private static final String TOPIC = "Kafka_Example";

	@GetMapping("/publish")
	public String post() {
		kafkaTemplate.send(TOPIC, new MedicineInfoEvent(RandomUtils.nextInt(100), RandomUtils.nextBoolean()));

		return "Published successfully";
	}
}
