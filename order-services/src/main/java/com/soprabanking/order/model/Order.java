package com.soprabanking.order.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.soprabanking.order.util.GlobalConstants;

@Document(collection = "order")
public class Order {
	
	@Id
	private int id;
	
	@NotEmpty
	@Size(min = 3)
	private String medicineName;
	
	@Min(value=1, message="The quantity must be equal or greater than 1")   
	private int quantity;
	
	private int totalAmount;
	
	@NotEmpty
	@Size(min = 3)
	private String userName;
	
	@NotEmpty
	@Size(min = 5)
	private String toAddress;
	
	private String orderStatus = GlobalConstants.DEFAULT_ORDER_STATUS;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMedicineName() {
		return medicineName;
	}
	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getToAddress() {
		return toAddress;
	}
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	@Override
	public String toString() {
		return "Order [id=" + id + ", medicineName=" + medicineName + ", quantity=" + quantity + ", totalAmount="
				+ totalAmount + ", userName=" + userName + ", toAddress=" + toAddress + ", orderStatus=" + orderStatus
				+ "]";
	}
	

}
