package com.soprabanking.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.soprabanking.order.exception.ResourceNotFoundException;
import com.soprabanking.order.model.Medicine;
import com.soprabanking.order.model.MedicineInfoEvent;
import com.soprabanking.order.model.Order;
import com.soprabanking.order.repository.OrderRepo;
import com.soprabanking.order.util.GlobalConstants;

import reactor.core.publisher.Mono;

@Service
public class OrderService {

	/*
	 * @Autowired private RestTemplate restTemplate;
	 */
	@Autowired
	private OrderRepo orderRepo;

	@Autowired
	private SequenceGeneratorService sequenceGeneratorService;

	private Medicine medicine;

	@Autowired
	private WebClient.Builder webClientBuilder;

	/**
	 * @param id
	 * @param order
	 * @return
	 * @throws ResourceNotFoundException
	 */
	public Mono<Order> saveOrder(int id, Order order) throws ResourceNotFoundException {

		order.setId(sequenceGeneratorService.generateSequence(GlobalConstants.ORDERS_SEQUENCE));

		medicine = webClientBuilder.build().get().uri(GlobalConstants.URL_GET_MEDICINE_BY_ID + id).retrieve()
				.bodyToMono(Medicine.class).block();

		/*
		 * medicine = restTemplate.getForObject(GlobalConstants.URL_GET_MEDICINE_BY_ID +
		 * id, Medicine.class);
		 */

		if (order.getQuantity() > medicine.getQuantity()) {
			System.out.println("Quantity exceeds availablity, order failed");
		}

		medicine.setQuantity(medicine.getQuantity() - order.getQuantity());
		/*
		 * webClientBuilder.build().put().uri(GlobalConstants.URL_UPDATE_MEDICINE + id,
		 * medicine).retrieve() .bodyToMono(Medicine.class).block();
		 */
		webClientBuilder.build().put().uri(GlobalConstants.URL_UPDATE_MEDICINE + id).body(Mono.just(medicine), Medicine.class)
		.retrieve().bodyToMono(Medicine.class).block();
		order.setTotalAmount(medicine.getAmount() * order.getQuantity());

		return orderRepo.save(order);
	}
	
	@KafkaListener(topics = "Kafka_Example", groupId = "group_id", containerFactory = "kafkaListenerContainerFactory")
	public void consumeJson(MedicineInfoEvent medicineInfo) {
		System.out.println("Consumed JSON Message: " + medicineInfo);
	}

}
