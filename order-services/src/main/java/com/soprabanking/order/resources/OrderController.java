package com.soprabanking.order.resources;

import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soprabanking.order.exception.ResourceNotFoundException;
import com.soprabanking.order.model.Order;
import com.soprabanking.order.repository.OrderRepo;
import com.soprabanking.order.service.OrderService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/pharmacy/order")
@CrossOrigin(origins = "http://localhost:4200")
public class OrderController {
	// Adding default slf4j logging
	Logger log = LoggerFactory.getLogger(OrderController.class);
	
	@Autowired
	private OrderRepo orderRepo;

	@Autowired
	private OrderService orderService;	
	
	@PostMapping("/addNewOrder/{id}")
	public Mono<Order> saveNewOrder(@PathVariable int id, @Valid @RequestBody Order order) throws ResourceNotFoundException {
		log.info("Order from: " + order.getUserName() + " has been requested for medicine: " + order.getMedicineName());
		return orderService.saveOrder(id, order);
	}

	@GetMapping("/getAllOrders")
	public Flux<Order> getOrder() {
		log.info("Order list is asked to be retrieved");
		return orderRepo.findAll();
	}

	@GetMapping("/getOrder/{id}")
	public Mono<Order> getOrder(@PathVariable int id) throws ResourceNotFoundException {
		log.info("Retrieve order with id: " + id);
		return Mono.just(id)
				.flatMap(orderRepo::findById)
				.switchIfEmpty(Mono.error(new ResourceNotFoundException("No order found for this id :: " + id)));
	}
	
	@GetMapping("/getOrdersByUserName/{userName}")
	public Flux<Order> getOrdersByUserName(@PathVariable String userName) {
		log.info("Retrieve all orders for the user: " + userName);
		return orderRepo.findOrdersByUserName(userName);
	}

	@PutMapping("/editOrder/{id}")
	public Mono<Order> updateOrder(@PathVariable int id, @RequestBody Order order) throws ResourceNotFoundException {
		log.info("Update order with id: " + id);
		Mono<Order> existingOrder = orderRepo.findById(id);
		if (Objects.isNull(existingOrder))
			return Mono.error(new ResourceNotFoundException("No order found for this id :: " + id));
		return orderRepo.save(order);
	}

	@DeleteMapping("/deleteOrder/{id}")
	public Mono<ResponseEntity<Void>> deleteOrder(@PathVariable int id) {
		log.info("Delete Order with id: " + id);
		return orderRepo.deleteById(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK)));
	}
}
